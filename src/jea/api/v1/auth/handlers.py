
"""
Django Rest Framework Authentication Handler Classes
"""

import logging

from rest_framework.exceptions import (
    AuthenticationFailed,
)
from rest_framework.authentication import (
    BaseAuthentication,
    SessionAuthentication,
    get_authorization_header,
)

from jea.crm.services import customers as customers_svc
from jea.auth.services import token_authentication as token_authentication_svc


logger = logging.getLogger(__name__)


class JWTAuthentication(BaseAuthentication):
    """
    Implement authentication with a given JWT token
    """
    def authenticate(self, request):
        """
        Validate token and load user.

        :raises: rest_framework.exceptions.AuthenticationFailed
        """
        auth_header = get_authorization_header(request).split()
        logger.debug("auth_header: %s", auth_header)

        # Check header
        if not auth_header or auth_header[0].lower() != b'bearer':
            return None, None

        logger.debug("bearer_token: present")

        # We have a bearer token
        if len(auth_header) != 2:
            raise AuthenticationFailed("Malformed authorization header.")

        # Try to decode the token
        claims = token_authentication_svc.decode(auth_header[1])

        # Get the customers from the
        customer = customers_svc.get_customer(
            customer=claims.get("sub"))
        if not customer:
            raise AuthenticationFailed(
                "Customer missing in token payload: `sub`")

        # Create ApiUser
        user = customer.user

        logger.debug(
            "authenticated: customer=%s, user=%s",
             customer, user,
        )

        return user, claims


class ApiSessionAuthentication(SessionAuthentication):
    """
    Session based authentication.

    This is more or less the same as the SessionAuthentication from
    rest_framework. However, we require a customer.

    !CAVEAT! This is only to be used in demonstration environments.
    """
    def authenticate(self, request):
        """
        Derive an API user from the current auth user.
        """
        result = super(ApiSessionAuthentication, self) \
            .authenticate(request)

        if not result:
            return None # Nothing to do here.

        if not request.session:
            return None # Sessions are disabled

        session = request.session

        # Let's make an APIUser
        customer = customers_svc.get_customer(
            customer=session.get("auth_customer_id"))
        if not customer:
            return None

        return customer.user, None

