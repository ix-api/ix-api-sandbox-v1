
"""
Common API.v1 views
"""

import traceback

from ixapi_schema.v1.entities import problems

from django.views.decorators.csrf import csrf_exempt
from django.utils import text as text_utils
from django.http import JsonResponse
from rest_framework import (
    exceptions as rest_exceptions,
    status,
)
from rest_framework.response import Response
from rest_framework.views import (
    exception_handler as framework_exception_handler,
)
from django.core import exceptions as django_exc
from jwt import exceptions as jwt_exc

from jea.api.v1.response import tags
from jea.api.v1.response.tags import tag_for_exception
from jea.api.v1.errors import handler as problem_handler

def _error_from_exception(exc, context):
    """
    Make error format representation from exception
    """
    exc_type = text_utils.camel_case_to_spaces(type(exc).__name__) \
        .replace(" ", "_")

    # Normalize naming
    if not exc_type.endswith("_error"):
        exc_type += "_error"

    return {
        "type": exc_type,
        "details": str(exc),
    }


def _api_exception_handler(exc, context):
    """
    Handle all custom exceptions, e.g. from jwt decoding
    """
    # Get exception parameters
    tag = tag_for_exception(exc)
    error = _error_from_exception(exc, context)

    # Exception to status
    response_status = status.HTTP_500_INTERNAL_SERVER_ERROR
    if isinstance(exc, jwt_exc.InvalidTokenError):
        response_status = status.HTTP_403_FORBIDDEN
    elif isinstance(exc, django_exc.ObjectDoesNotExist):
        response_status = status.HTTP_404_NOT_FOUND

    # In case we still have some 'generic exception',
    # print traceback.
    if response_status == status.HTTP_500_INTERNAL_SERVER_ERROR:
        traceback.print_exc()


    # Make response
    response = Response(status=response_status, data=error)
    response.tag = tag

    return response


def exception_handler(exc, context):
    """
    Custom exception handler to wrap exceptions in the
    response envelope and use status.errors and status.tag
    """
    return problem_handler.handle_exception(exc, context)


@csrf_exempt
def resource_not_found(request, path=None):
    """Handle missing resources"""
    detail = "The resource '/api/v1/{}' could not be found.".format(path)
    exc = rest_exceptions.NotFound(detail)
    return problem_handler.handle_exception(exc, None)
