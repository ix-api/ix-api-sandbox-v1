
"""
Catalog :: Views

Implements catalog related endpoints. For example
for listing facilities and points of presence.
"""

from rest_framework import exceptions, status
from ixapi_schema.v1.entities import catalog, problems

from jea.api.v1.permissions import require_customer
from jea.api.v1.response import tags
from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.response import ApiSuccess, ApiError
from jea.catalog.services import (
    devices as devices_svc,
    pops as pops_svc,
    products as products_svc,
    facilities as facilities_svc,
)


class FacilitiesViewSet(JEAViewSet):
    """
    A `Facility` is a data centre, with a determined physical address,
    from which a defined set of PoPs can be accessed
    """
    def list(self, request):
        """Get a (filtered) list of `facilities`."""
        facilities = facilities_svc.get_facilities(
            filters=request.query_params)
        return catalog.Facility(facilities, many=True).data

    def retrieve(self, request, pk=None):
        """Retrieve a facility by id"""
        facility = facilities_svc.get_facility(facility=pk)
        return catalog.Facility(facility).data


class DevicesViewSet(JEAViewSet):
    """
    A `Device` is a network hardware device, typically a Switch, which
    is located at a specified facility and connected to one or more
    PoPs.

    They may be physically located at their related PoPs or remotely
    available.
    """
    def list(self, request):
        """List available devices"""
        devices = devices_svc.get_devices(
            filters=request.query_params)
        return catalog.Device(devices, many=True).data

    def retrieve(self, request, pk=None):
        """Get a specific device identified by id"""
        device = devices_svc.get_device(device=pk)
        return catalog.Device(device).data


class PointsOfPresenceViewSet(JEAViewSet):
    """
    A `PoP` is a location within a Facility which is connected to a
    single Network Infrastructure and has defined reachability of other
    facilities.

    A single room may contain multiple PoPs, each linking to a different
    infrastructure.
    """
    def list(self, request):
        """List all PoPs"""
        pops = pops_svc.get_pops(filters=request.query_params)
        return catalog.PointOfPresence(pops, many=True).data

    def retrieve(self, request, pk=None):
        """Get a demarcation point"""
        pop = pops_svc.get_pop(pop=pk)
        return catalog.PointOfPresence(pop).data


class ProductsViewSet(JEAViewSet):
    """
    A `Product` is a network or peering-related product of a defined
    type sold by an IXP to its `Customers`
    """
    def list(self, request):
        """List all (filtered) products available on the platform"""
        products = products_svc.get_products(filters=request.query_params)
        return catalog.Product(products, many=True).data

    def retrieve(self, request, pk=None):
        """Get a specific product by id"""
        product = products_svc.get_product(product=pk)
        return catalog.Product(product).data
