
import pytest
from model_bakery import baker
from ixapi_schema.v1.entities import access

from jea.api.v1.access.serializers import get_config_update_serializer


@pytest.mark.django_db
def test_get_config_update_serializer():
    """Get serializer based on config type"""
    # Network Services
    #  - Exchange Lan
    config = baker.make("access.ExchangeLanNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.ExchangeLanNetworkServiceConfigUpdate

    #  - Closed User Group
    config = baker.make("access.ClosedUserGroupNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.ClosedUserGroupNetworkServiceConfigUpdate

    #  - ELine
    config = baker.make("access.ELineNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.ELineNetworkServiceConfigUpdate

    #  - Cloud
    config = baker.make("access.CloudNetworkServiceConfig")
    assert get_config_update_serializer(config) == \
        access.CloudNetworkServiceConfigUpdate

    # Network Features
    #  - Blackholing
    config = baker.make("access.BlackholingNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        access.BlackholingNetworkFeatureConfigUpdate

    #  - Route Server
    config = baker.make("access.RouteServerNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        access.RouteServerNetworkFeatureConfigUpdate

    #  - IXP Router
    config = baker.make("access.IXPRouterNetworkFeatureConfig")
    assert get_config_update_serializer(config) == \
        access.IXPRouterNetworkFeatureConfigUpdate

    with pytest.raises(TypeError):
        config = baker.make("access.NetworkServiceConfig")
        get_config_update_serializer(config)
