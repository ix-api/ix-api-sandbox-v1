
"""
Test IX-API Access Entities
"""

import pytest
from model_bakery import baker
from ixapi_schema.v1.entities import access

from jea.api.v1.access.serializers import (
    get_config_update_serializer,
)


@pytest.mark.django_db
def test_connection_serialization():
    """Test serialization of a connection"""
    conn = baker.make("access.Connection")
    demarc = baker.make("access.DemarcationPoint", connection=conn)

    serializer = access.Connection(conn)
    data = serializer.data

    assert data, "Serializer should serialize without crashing"
    assert str(demarc.id) in data["demarcs"]


@pytest.mark.django_db
def test_connection_request_serializer():
    """Test create connection request serializer"""
    customer = baker.make("crm.Customer")
    contact = baker.make("crm.ImplementationContact")

    payload = {
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "mode": "lag_lacp",
        "lacp_timeout": "slow",
        "contacts": [contact.pk],
    }

    serializer = access.ConnectionRequest(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_connection_update_serializer():
    """Test create connection request serializer"""
    customer = baker.make("crm.Customer")
    contact = baker.make("crm.ImplementationContact")

    payload = {
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "mode": "lag_static",
        "contacts": [contact.pk],
    }

    serializer = access.ConnectionUpdate(data=payload)
    serializer.is_valid()
    assert not serializer.errors


@pytest.mark.django_db
def test_polymorphic_network_service_config_serializer():
    """Test Polymorphic network service config serializer"""
    config = baker.make("access.ExchangeLanNetworkServiceConfig")
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "exchange_lan"

    config = baker.make("access.ClosedUserGroupNetworkServiceConfig")
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "closed_user_group"

    config = baker.make("access.ELineNetworkServiceConfig")
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "eline"

    config = baker.make("access.CloudNetworkServiceConfig")
    serializer = access.NetworkServiceConfig(config)

    assert serializer.data
    assert serializer.data["type"] == "cloud"


@pytest.mark.django_db
def test_polymorphic_network_service_config_deserialization():
    """Test deserialization of network service input"""
    customer = baker.make("crm.Customer")
    conn = baker.make("access.Connection")
    service = baker.make("service.ExchangeLanNetworkService")
    contact = baker.make("crm.ImplementationContact")
    mac = baker.make("ipam.MacAddress")

    data = {
        "type": "exchange_lan",
        "capacity": None,
        "contacts": [contact.pk],
        "connection": conn.pk,
        "network_service": service.pk,
        "asns": [2342],
        "macs": [mac.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "outer_vlan": [[42]],
    }

    serializer = access.NetworkServiceConfigRequest(data=data)
    serializer.is_valid()
    assert not serializer.errors
    assert serializer.validated_data["type"] == "exchange_lan"

    data = {
        "type": "exchange_lan",
        "capacity": 1,
        "contacts": [contact.pk],
        "asns": [2342],
        "outer_vlan": [[0, 4095]],
        "connection": "",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = access.NetworkServiceConfig(data=data)
    assert not serializer.is_valid()


@pytest.mark.django_db
def test_demarcation_point_serialization():
    """Test serialization of a port demarc"""
    demarc = baker.make("access.DemarcationPoint")
    serializer = access.DemarcationPoint(demarc)

    data = serializer.data
    assert data
    assert isinstance(data["contacts"], list)


@pytest.mark.django_db
def test_demarcation_point_request_serializer():
    """Test demarcation point request serializer"""
    connection = baker.make("access.Connection")
    customer = baker.make("crm.Customer")
    contact = baker.make("crm.ImplementationContact")
    pop = baker.make("catalog.PointOfPresence")

    payload = {
        "pop": pop.pk,
        "media_type": "100GBASE-LR",
        "connection": connection.pk,
        "contacts": [contact.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = access.DemarcationPointRequest(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_demarcation_point_update_serializer():
    """Test demarcation point update serializer"""
    connection = baker.make("access.Connection")
    customer = baker.make("crm.Customer")
    contact = baker.make("crm.ImplementationContact")

    payload = {
        "connection": connection.pk,
        "contacts": [contact.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    }

    serializer = access.DemarcationPointUpdate(data=payload)
    assert serializer.is_valid()


@pytest.mark.django_db
def test_feature_config_serialization():
    """Test serialization of feature config objects"""
    service_config = baker.make("access.ExchangeLanNetworkServiceConfig")
    feature = baker.make("service.RouteServerNetworkFeature")
    feature_config = baker.make(
        "access.RouteServerNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = access.NetworkFeatureConfig(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "route_server"

    feature = baker.make("service.IXPRouterNetworkFeature")
    feature_config = baker.make(
        "access.IXPRouterNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = access.NetworkFeatureConfig(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "ixp_router"

    feature = baker.make("service.BlackholingNetworkFeature")
    feature_config = baker.make(
        "access.BlackholingNetworkFeatureConfig",
        network_service_config=service_config,
        network_feature=feature)
    serializer = access.NetworkFeatureConfig(feature_config)
    assert serializer.data, "Serializer should serialize without crashing"
    assert serializer.data["type"] == "blackholing"


@pytest.mark.django_db
def test_network_service_config_udpate_base_serializer():
    """Test service config update base serializer"""
    customer = baker.make("crm.Customer")
    impl = baker.make("crm.ImplementationContact")
    connection = baker.make("access.Connection")

    serializer = access.NetworkServiceConfigUpdateBase(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_exchange_lan_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    customer = baker.make("crm.Customer")
    impl = baker.make("crm.ImplementationContact")
    connection = baker.make("access.Connection")
    mac = baker.make("ipam.MacAddress")

    serializer = access.ExchangeLanNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "macs": [mac.pk],
        "asns": [42],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_closed_user_group_config_update_serializer():
    """Test update serializer for network, service config"""
    customer = baker.make("crm.Customer")
    impl = baker.make("crm.ImplementationContact")
    connection = baker.make("access.Connection")
    mac = baker.make("ipam.MacAddress")

    serializer = access.ClosedUserGroupNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "macs": [mac.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_eline_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    customer = baker.make("crm.Customer")
    impl = baker.make("crm.ImplementationContact")
    connection = baker.make("access.Connection")

    serializer = access.ELineNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })


    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_cloud_network_service_config_update_serializer():
    """Test update serializer for network service config"""
    customer = baker.make("crm.Customer")
    impl = baker.make("crm.ImplementationContact")
    connection = baker.make("access.Connection")

    serializer = access.CloudNetworkServiceConfigUpdate(data={
        "contacts": [impl.pk],
        "connection": connection.pk,
        "cloud_key": "f000000",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_blackholing_network_feature_config_update_serializer():
    """Test update serializer for feature config: blackholing"""
    customer = baker.make("crm.Customer")
    prefix = baker.make("ipam.IpAddress")
    serializer = access.BlackholingNetworkFeatureConfigUpdate(data={
        "purchase_order": "fooooooo",
        "filtered_prefixes": [prefix.pk],
        "ixp_specific_configuration": "DELETE FROM `peers` WHERE 1",
        "activated": True,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_route_server_network_feature_config_update_serializer():
    """Test update serializer for route server feature configs"""
    customer = baker.make("crm.Customer")
    serializer = access.RouteServerNetworkFeatureConfigUpdate(data={
        "asn": 2342,
        "as_set": "AS-FOO",
        "max_prefix_v4": 5000,
        "insert_ixp_asn": True,
        "session_mode": "public",
        "bgp_session_type": "active",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)


@pytest.mark.django_db
def test_ixp_router_network_feature_config_update_serializer():
    """Test update serializer for ixp router feature config"""
    customer = baker.make("crm.Customer")
    serializer = access.IXPRouterNetworkFeatureConfigUpdate(data={
        "max_prefix": 23,
        "bgp_session_type": "active",
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    assert serializer.is_valid(raise_exception=True)

