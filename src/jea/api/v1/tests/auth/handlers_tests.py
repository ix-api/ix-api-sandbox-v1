
import pytest
from model_bakery import baker

from rest_framework.test import APIRequestFactory

from jea.auth.models import User
from jea.auth.services import token_authentication as token_authentication_svc
from jea.api.v1.auth.handlers import (
    JWTAuthentication,
    ApiSessionAuthentication,
)



@pytest.mark.django_db
def test_jwt_authentication_authenticate():
    """Test authenticating a request"""
    # Create customer and subcustomer
    customer = baker.make("crm.Customer")
    baker.make(User, customer=customer)

    access_token, _ = token_authentication_svc.issue_tokens(customer.pk)

    # Make request
    request = APIRequestFactory().get("/api/example")
    request.META["HTTP_AUTHORIZATION"] = b"Bearer " + \
        access_token.encode('utf-8')

    # Authenticate request
    handler = JWTAuthentication()
    user, token = handler.authenticate(request)

    assert user.customer.pk == customer.pk


@pytest.mark.django_db
def test_api_session_authentication_authenticate():
    """Test authenticating a request"""
    # Create customer and subcustomer
    customer = baker.make("crm.Customer")
    user = baker.make("jea_auth.User", customer=customer)

    # Make request
    request = APIRequestFactory().get("/api/example")
    request.user = user
    request.session = {
        "auth_customer_id": customer.id,
    }
    request._request = request

    # Authenticate request
    handler = ApiSessionAuthentication()
    result = handler.authenticate(request)
    assert result

    user, _ = result

    assert user.customer.id == customer.id

