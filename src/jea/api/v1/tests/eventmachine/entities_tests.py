
import pytest
from model_bakery import baker
from ixapi_schema.v1.entities import events

@pytest.mark.django_db
def test_status_serializer():
    """Test status serializer"""
    status = baker.prepare("eventmachine.StatusMessage")
    serializer = events.Status(status)

    assert serializer.data, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_event_serializer():
    """Test status serializer"""
    event = baker.prepare("eventmachine.Event")
    serializer = events.Event(event)

    assert serializer.data, "Serializer should serialize without crashing"

