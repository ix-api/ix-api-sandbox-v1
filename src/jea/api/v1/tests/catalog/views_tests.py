
"""
Test Catalog Views
"""

import pytest
from model_bakery import baker
from rest_framework import test

from jea.api.v1.tests import authorized_requests
from jea.api.v1.catalog import views


@pytest.mark.django_db
def test_facilities_view_set__list():
    """Test list all facilities"""
    customer = baker.make("crm.Customer")
    view = views.FacilitiesViewSet.as_view({"get": "list"})

    # Make request
    request = authorized_requests.get(customer)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_facilities_view_set__retrieve():
    """Test retrieving a facility"""
    customer = baker.make("crm.Customer")
    facility = baker.make("catalog.Facility")

    # Make request
    view = views.FacilitiesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)
    response = view(request, pk=facility.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__list():
    """Test listing devices"""
    customer = baker.make("crm.Customer")
    view = views.DevicesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__retrieve():
    """Test retrieving a device"""
    customer = baker.make("crm.Customer")
    device = baker.make("catalog.Device")

    view = views.DevicesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)

    response = view(request, pk=device.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_points_of_presence_view_set__list():
    """Test getting all pops"""
    customer = baker.make("crm.Customer")
    request = authorized_requests.get(customer)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_points_of_presence_view_set__retrieve():
    """Test getting a specific device"""
    customer = baker.make("crm.Customer")
    pop = baker.make("catalog.PointOfPresence")
    request = authorized_requests.get(customer)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "retrieve",
    })

    response = view(request, pk=pop.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_products_view_set__list():
    """Test getting all the products"""
    customer = baker.make("crm.Customer")
    request = authorized_requests.get(customer)
    view = views.ProductsViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_products_view_set__retrieve():
    """Test getting a specific product"""
    customer = baker.make("crm.Customer")
    product = baker.make("catalog.ExchangeLanNetworkProduct")
    request = authorized_requests.get(customer)
    view = views.ProductsViewSet.as_view({"get": "retrieve"})

    response = view(request, pk=product.pk)
    assert response.status_code == 200


