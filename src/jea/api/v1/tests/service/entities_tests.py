
"""
Test Service Serializers
"""

import pytest
from model_bakery import baker
from ixapi_schema.v1.entities import service

#
# Helper: Create a service with product
#
def _make_service(service_model, product_model):
    """Create a service with a product"""
    product = baker.make(product_model)
    network_service = baker.make(
        service_model,
        product=product,
        required_contact_types=["noc"])

    return network_service


@pytest.mark.django_db
def test_exchange_lan_network_serializer():
    """
    Test serialization of exchange lan network services
    """
    network_service = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProduct",
    )
    serializer = service.ExchangeLanNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check embedded / inlines
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_closed_user_group_serializer():
    """
    Test serialization of closed user groups
    """
    network_service = _make_service(
        "service.ClosedUserGroupNetworkService",
        "catalog.ClosedUserGroupNetworkProduct",
    )
    serializer = service.ClosedUserGroupNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_eline_serializer():
    """
    Test serialization of eline network services
    """
    network_service = _make_service(
        "service.ELineNetworkService",
        "catalog.ELineNetworkProduct",
    )
    serializer = service.ELineNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_cloud_network_service_serializer():
    """
    Test serialization of cloud network services
    """
    network_service = _make_service(
        "service.CloudNetworkService",
        "catalog.CloudNetworkProduct",
    )
    serializer = service.ClosedUserGroupNetworkService(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_polymorphic_network_service_serializer_serialize():
    """
    Test the polymorphic network service serializer
    serialization method.
    """
    exchange_lan = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProduct",
    )
    closed_user_group = _make_service(
        "service.ClosedUserGroupNetworkService",
        "catalog.ClosedUserGroupNetworkProduct",
    )
    eline = _make_service(
        "service.ELineNetworkService",
        "catalog.ELineNetworkProduct",
    )
    cloud = _make_service(
        "service.CloudNetworkService",
        "catalog.CloudNetworkProduct",
    )

    services = [exchange_lan, closed_user_group, eline, cloud]
    serializer = service.NetworkService(services, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing."

#
# Features
#
def test_ixp_specific_feature_flag_serializer():
    """Test feature flag serialization"""
    feature_flag = baker.prepare("service.IXPSpecificFeatureFlag")
    serializer = service.IXPSpecificFeatureFlag(feature_flag)
    assert serializer.data, "Serializer should serialize without crashing"


def test_feature_base_serializer():
    """Test base serializer for features"""
    feature = baker.prepare("service.NetworkFeature")
    serializer = service.NetworkFeatureBase(feature)
    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check embeddings
    assert isinstance(result["flags"], list)


def test_blackholing_feature_serializer():
    """Test serializing a blackholing feature"""
    feature = baker.prepare("service.BlackholingNetworkFeature")
    serializer = service.BlackholingNetworkFeature(feature)
    assert serializer.data, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_route_server_feature_serializer():
    """Test route server feature serialization"""
    ip4 = baker.make("ipam.IpAddress", version=4)
    feature = baker.make("service.RouteServerNetworkFeature",
                         ip_addresses=[ip4])
    serializer = service.RouteServerNetworkFeature(feature)
    result = serializer.data

    assert result, "Serializer should produce data"

    # Check embeddings
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_ixprouter_feature_serializer():
    """Test IXPRouter feature serialization"""
    ip4 = baker.make("ipam.IpAddress", version=4)
    feature = baker.make("service.IXPRouterNetworkFeature",
                         ip_addresses=[ip4])
    serializer = service.IXPRouterNetworkFeature(feature)
    result = serializer.data

    assert result, "Serializer should produce data"

    # Check embeddings
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_polymorphic_feature_serializer():
    """Test polymorphic feature serialization"""
    features = [
        baker.make("service.BlackholingNetworkFeature"),
        baker.make("service.IXPRouterNetworkFeature"),
        baker.make("service.RouteServerNetworkFeature"),
    ]

    serializer = service.NetworkFeature(features, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing"
    assert len(result) == len(features), \
        "Serializer should serialize all features"

