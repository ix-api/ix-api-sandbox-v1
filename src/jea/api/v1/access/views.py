
"""
JEA Access :: Views

Implement views for access objects like network service accesses and
feature accesses.
"""

from rest_framework import exceptions, status
from ixapi_schema.v1.entities import access, problems

from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.response import ApiSuccess, ApiError
from jea.api.v1.permissions import (
    require_customer,
)
from jea.api.v1.access.serializers import get_config_update_serializer
from jea.access.services import (
    demarcs as demarcs_svc,
    connections as connections_svc,
    configs as configs_svc,
)
from jea.eventmachine.models import State


#
# Demarcation Points
#
class DemarcsViewSet(JEAViewSet):
    """
    A `Demarc` (demarcation point) is the point at which customer and
    IXP networks meet, eg a physical port / socket, generally with a
    specified bandwidth.

    Demarcs are listed on a LoA (Letter of Authorisation).
    Exchange customers need this information to order a cross connect
    from the datacenter operator to be interconnected to the exchange.

    Due to the reason a `demarc` is patched to a switch, it comes with
    necessary extra informations like speed and optics `type`.
    A `demarc` is always associated to one `pop`.
    """
    @require_customer
    def list(self, request, customer=None):
        """List all `demarc`s."""
        # Do not include archived demarcs unless requested
        filters = request.query_params.copy()
        if not filters.get("state") and not filters.get("state__is_not"):
            filters["state__is_not"] = "archived"

        demarcs = demarcs_svc.get_demarcation_points(
            scoping_customer=customer,
            filters=filters)

        serializer = access.DemarcationPoint(demarcs, many=True)
        return serializer.data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Read a `demarc`."""
        demarc = demarcs_svc.get_demarcation_point(
            scoping_customer=customer,
            demarcation_point=pk,
        )

        serializer = access.DemarcationPoint(demarc)
        return serializer.data

    @require_customer
    def create(self, request, customer=None):
        """
        Create a `demarc`.
        """
        serializer = access.DemarcationPointRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        demarc = demarcs_svc.allocate_demarcation_point(
            scoping_customer=customer,
            demarcation_point_input=serializer.validated_data)

        # Serialize new port demarc
        serializer = access.DemarcationPoint(demarc)
        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_customer
    def update(self, request, customer=None, pk=None):
        """
        Update a `demarc`.
        """
        serializer = access.DemarcationPointUpdate(data=request.data)
        serializer.is_valid(raise_exception=True)

        demarc = demarcs_svc.update_demarcation_point(
            scoping_customer=customer,
            demarcation_point=pk,
            demarcation_point_update=serializer.validated_data)

        # Serialize the result
        serializer = access.DemarcationPoint(demarc)
        return ApiSuccess(serializer.data)

    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """
        Partially update a `demarc`.
        """
        serializer = access.DemarcationPointUpdate(
            data=request.data,
            partial=True)
        serializer.is_valid(raise_exception=True)

        demarc = demarcs_svc.update_demarcation_point(
            scoping_customer=customer,
            demarcation_point=pk,
            demarcation_point_update=serializer.validated_data)

        # Serialize the result
        serializer = access.DemarcationPoint(demarc)
        return ApiSuccess(serializer.data)

    @require_customer
    def destroy(self, request, customer=None, pk=None):
        """
        Delete a `demarc`.
        """
        demarc = demarcs_svc.get_demarcation_point(
            scoping_customer=customer,
            demarcation_point=pk,
        )

        if demarc.state == State.ARCHIVED:
            pass
        elif demarc.state == State.DECOMMISSIONED:
            demarc = demarcs_svc.archive_demarcation_point(
                demarcation_point=demarc)
        else:
            demarc = demarcs_svc.release_demarcation_point(
                demarcation_point=demarc)

        serializer = access.DemarcationPoint(demarc)
        return serializer.data


class ConnectionsViewSet(JEAViewSet):
    """
    A `Connection` is a functional group of physical connections
    collected together into a LAG (aka trunk).

    A `connection` with only one `demarc` can be also configured as
    standalone which means no LAG configuration on the switch.
    """
    @require_customer
    def list(self, request, customer=None):
        """List all `connection`s."""
        # Filter archived if no state filter is present
        filters = request.query_params.copy()
        if not filters.get("state") and not filters.get("state__is_not"):
            filters["state__is_not"] = "archived"

        connections = connections_svc.get_connections(
            scoping_customer=customer,
            filters=filters)

        serializer = access.Connection(connections, many=True)
        return serializer.data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """
        Read a `connection`.
        """
        connection = connections_svc.get_connection(
            scoping_customer=customer,
            connection=pk)

        serializer = access.Connection(connection)
        return serializer.data

    @require_customer
    def create(self, request, customer=None):
        """
        Create a new `connection`.
        """
        serializer = access.ConnectionRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Everything good? Let's create the connection
        connection = connections_svc.create_connection(
            scoping_customer=customer,
            connection_input=serializer.validated_data)

        serializer = access.Connection(connection)

        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_customer
    def update(self, request, customer=None, pk=None):
        """Update a `connection`."""
        serializer = access.ConnectionUpdate(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Allright - then let's proceed with the update
        connection = connections_svc.update_connection(
            scoping_customer=customer,
            connection=pk,
            connection_update=serializer.validated_data)

        serializer = access.Connection(connection)

        return ApiSuccess(serializer.data)

    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """Partially update a `connection`."""
        serializer = access.ConnectionUpdate(
            data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Allright - then let's proceed with the update
        connection = connections_svc.update_connection(
            scoping_customer=customer,
            connection=pk,
            connection_update=serializer.validated_data)

        serializer = access.Connection(connection)

        return ApiSuccess(serializer.data)

    @require_customer
    def destroy(self, request, customer=None, pk=None):
        """
        Delete a `connection`.
        """
        connection = connections_svc.archive_connection(
            scoping_customer=customer,
            connection=pk)

        return access.Connection(connection).data

#
# Configurations
#
class NetworkServiceConfigsViewSet(JEAViewSet):
    """
    A `NetworkServiceConfig` is a customer's configuration for usage
    of a `NetworkService`, eg the configuration of a (subset of a)
    connection for that customer's traffic

    The `type` of the config determines the service you are
    configuring.

    You can find the services available to you on the platform,
    by querying the `/api/v1/network-services` resource.
    """
    @require_customer
    def list(self, request, customer=None):
        """Get all `network-service-config`s."""
        service_configs = configs_svc.get_network_service_configs(
            scoping_customer=customer,
            filters=request.query_params)

        return access.NetworkServiceConfig(service_configs, many=True).data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Get a `network-service-config`"""
        service_config = configs_svc.get_network_service_config(
            scoping_customer=customer,
            network_service_config=pk)

        return access.NetworkServiceConfig(service_config).data

    @require_customer
    def update(self, request, customer=None, pk=None):
        """Update an exisiting `network-service-config`"""
        service_config = configs_svc.get_network_service_config(
            scoping_customer=customer,
            network_service_config=pk)

        serializer_class = get_config_update_serializer(service_config)
        serializer = serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Perform update
        service_config = configs_svc.update_network_service_config(
            scoping_customer=customer,
            network_service_config=service_config,
            network_service_config_update=serializer.validated_data)

        # Render result
        return access.NetworkServiceConfig(service_config).data

    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """Update parts of an exisiting `network-service-config`"""
        service_config = configs_svc.get_network_service_config(
            scoping_customer=customer,
            network_service_config=pk)

        serializer_class = get_config_update_serializer(service_config)
        serializer = serializer_class(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Perform update
        service_config = configs_svc.update_network_service_config(
            scoping_customer=customer,
            network_service_config=service_config,
            network_service_config_update=serializer.validated_data)

        # Render result
        return access.NetworkServiceConfig(service_config).data

    @require_customer
    def create(self, request, customer=None):
        """Create a `network-service-config`."""
        serializer = access.NetworkServiceConfigRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        # We now have validated configuration input,
        # now let the configs service create a new configuration
        service_config = configs_svc.create_network_service_config(
            scoping_customer=customer,
            network_service_config_input=serializer.validated_data)

        serializer = access.NetworkServiceConfig(service_config)
        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)


    @require_customer
    def destroy(self, request, customer=None, pk=None):
        """Deconfigure the network service"""
        service_config = configs_svc.destroy_network_service_config(
            scoping_customer=customer,
            network_service_config=pk)

        return access.NetworkServiceConfig(service_config).data


class NetworkFeatureConfigsViewSet(JEAViewSet):
    """
    A `NetworkFeatureConfig` is a customer's configuration for usage of
    a `NetworkFeature`
    """
    @require_customer
    def list(self, request, customer=None):
        """Get all network feature configs."""
        feature_configs = configs_svc.get_network_feature_configs(
            scoping_customer=customer,
            filters=request.query_params)

        return access.NetworkFeatureConfig(feature_configs, many=True).data

    @require_customer
    def retrieve(self, request, customer=None, pk=None):
        """Get a single network feature config"""
        feature_config = configs_svc.get_network_feature_config(
            scoping_customer=customer,
            network_feature_config=pk)

        return access.NetworkFeatureConfig(feature_config).data

    @require_customer
    def update(self, request, customer=None, pk=None):
        """Update a network feature configuration"""
        feature_config = configs_svc.get_network_feature_config(
            scoping_customer=customer,
            network_feature_config=pk)

        # Deserialize request
        serializer_class = get_config_update_serializer(feature_config)
        serializer = serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Update feature config using service
        feature_config = configs_svc.update_network_feature_config(
            scoping_customer=customer,
            network_feature_config=feature_config,
            network_feature_config_update=serializer.validated_data)

        # Respond with result
        return access.NetworkFeatureConfig(feature_config).data

    @require_customer
    def partial_update(self, request, customer=None, pk=None):
        """Update parts of the network feature config"""
        feature_config = configs_svc.get_network_feature_config(
            scoping_customer=customer,
            network_feature_config=pk)

        # Deserialize request
        serializer_class = get_config_update_serializer(feature_config)
        serializer = serializer_class(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        # Update feature config using service
        feature_config = configs_svc.update_network_feature_config(
            scoping_customer=customer,
            network_feature_config=feature_config,
            network_feature_config_update=serializer.validated_data)

        # Respond with result
        return access.NetworkFeatureConfig(feature_config).data

    @require_customer
    def create(self, request, customer=None):
        """
        Create a new feature configuration.

        Remeber to provide a feature `type` and the id of the
        `network_feature` you want to configure.
        Additionally you have to provide the `network_service_config`
        where you want to use the network feature.

        You can query the available features from the
        `/api/v1/network-features` resource.
        """
        serializer = access.NetworkFeatureConfigRequest(data=request.data)
        serializer.is_valid(raise_exception=True)

        feature_config = configs_svc.create_network_feature_config(
            scoping_customer=customer,
            network_feature_config_input=serializer.validated_data)

        serializer = access.NetworkFeatureConfig(feature_config)
        return ApiSuccess(serializer.data, status=status.HTTP_201_CREATED)

    @require_customer
    def destroy(self, request, customer=None, pk=None):
        """Remove a network feature config"""
        feature_config = configs_svc.destroy_network_feature_config(
            scoping_customer=customer,
            network_feature_config=pk)

        serializer = access.NetworkFeatureConfig(feature_config)
        return ApiSuccess(serializer.data)

