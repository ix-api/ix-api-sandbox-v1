# from django.conf.urls import url, handler404
from django.urls import path, include, re_path

from jea.api.ctrl import views as ctrl_views


urlpatterns = [
    path("reset-<token>", ctrl_views.reset_trigger),
]


