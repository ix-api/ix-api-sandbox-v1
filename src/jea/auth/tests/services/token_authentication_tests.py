
"""
Token Authentication Service Tests
"""

from datetime import datetime, timedelta

import pytest
from model_bakery import baker
from freezegun import freeze_time
from jwt.exceptions import InvalidTokenError

from jea.auth import roles, models as auth_models
from jea.auth.services import token_authentication as token_authentication_svc
from jea.crm import models as crm_models


def test_issue_token():
    """Test JWT creation"""
    customer = baker.prepare(crm_models.Customer)
    token = token_authentication_svc.issue_token(
        customer.pk, lifetime=timedelta(minutes=5))

    assert token


def test_issue_tokens():
    """Test token pair creation"""
    customer = baker.prepare(crm_models.Customer)
    access, refresh = token_authentication_svc.issue_tokens(
        customer.pk)

    # Decode tokens and inspect payload
    access_payload = token_authentication_svc.decode(access)
    refresh_payload = token_authentication_svc.decode(refresh)

    assert roles.ACCESS in access_payload["roles"]
    assert roles.TOKEN_REFRESH in refresh_payload["roles"]


def test_decode_validation():
    """Test token validation"""
    customer = baker.prepare(crm_models.Customer)

    with freeze_time() as t:
        # T0: Create fresh token with a lifetime of 30 minutes
        token = token_authentication_svc.issue_token(
            customer.id, lifetime=timedelta(minutes=30))

        # T0+10m:
        # This should be valid and decodable
        t.tick(delta=timedelta(minutes=10))
        assert not token_authentication_svc.decode(token) is None

        # T0+31m:
        # The token should have expired
        t.tick(delta=timedelta(minutes=21))
        with pytest.raises(InvalidTokenError) as exc:
            token_authentication_svc.decode(token)


@pytest.mark.django_db
def test_authenticate_empty_customers():
    """Authenticate with valid credentials"""
    customer = baker.make("crm.Customer")
    user = baker.make(auth_models.User,
                      customer=customer)

    access_token, refresh_token = token_authentication_svc.authenticate_api_key(
        user.api_key, user.api_secret)

    # Assertations
    assert access_token # Not empty
    assert refresh_token # Not empty

    # Check content
    payload = token_authentication_svc.decode(access_token)
    assert payload["sub"], \
        "Customer should be set"
    assert str(payload["sub"]) == str(user.customer.pk), \
        "JWT token subject should be the assigned customer"

