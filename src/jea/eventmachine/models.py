
"""
Event Machine Models

Describe object state, status and actions.
Also Operations are described here.

Operations are tasks that can be scheduled to a
specific timeframe and can be executed by an agaent.
These are either sync- or asynchronous.

Agents however can be anything that are able to perform
a given task / operation. This includes software services,
human beings, robots and so on.
"""

from enum import Enum

import enumfields
from django.db import models
from django.contrib.contenttypes import fields as contenttype_fields
from django.contrib.contenttypes import models as contenttype_models
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import timezone



class State(Enum):
    """Enum containing all states the statemachine can transition"""
    REQUESTED = "requested"
    ALLOCATED = "allocated"

    TESTING = "testing"
    PRODUCTION = "production"

    DECOMISSION_REQUESTED = "decommission_requested"
    DECOMMISSIONED = "decommissioned"

    ARCHIVED = "archived"

    ERROR = "error"
    OPERATOR = "operator"
    SCHEDULED = "scheduled" # I'm not convinced about this.


class Severity:
    """Status Severity"""
    EMERGENCY = 0
    ALERT = 1
    CRITICAL = 2
    ERROR = 3
    WARNING = 4
    NOTICE = 5
    INFORMATIONAL = 6
    DEBUG = 7


def _initial_payload():
    """Create empty payload object to prevent shared objects"""
    return {}


class Event(models.Model):
    """
    Events are emitted by operations.
    Whenever some change in the state occures, an event
    should be dispatched.
    """
    serial = models.AutoField(primary_key=True)
    timestamp = models.DateTimeField(default=timezone.now)

    type = models.CharField(max_length=80)
    payload = models.JSONField(encoder=DjangoJSONEncoder,
                               default=_initial_payload)
    # References:
    # We make a polymorphic relation to the object we
    # are tracking events for.
    ref_type = models.ForeignKey(contenttype_models.ContentType,
                                 on_delete=models.CASCADE,
                                 null=True, blank=True)
    ref_id = models.PositiveIntegerField(null=True, blank=True)
    ref = contenttype_fields.GenericForeignKey('ref_type', 'ref_id')


    # We create event streams based on the customer scope:
    # A customer can see his events and the events regarding the
    # subcustomer. In that case the reseller, does not create
    # a listener for each customer.
    #
    # We are using the `scoping_customer` for this, which
    # might not be present. In that case, the event is recorded,
    # however it will never appear in an event stream.
    customer = models.ForeignKey(
        "crm.Customer",
        null=True,
        on_delete=models.CASCADE)

    class Meta:
        ordering = ["serial"]

    def __repr__(self):
        """Make representation"""
        return (f"<Event serial={self.serial} type={self.type}"
                f" ref_type={self.ref_type} ref_id={self.ref_id}>")

    def __str__(self):
        """Make string representation"""
        return (f"Event [serial={self.serial} type={self.type}"
                f" ref_type={self.ref_type} ref_id={self.ref_id}]")


class StatusMessage(models.Model):
    """
    Multiple messages can be assigned to an object
    representing additional status information
    e.g. why the current state is an error state.
    """
    severity = models.PositiveSmallIntegerField(default=1)

    message = models.CharField(max_length=255)
    tag = models.CharField(max_length=80)

    # Message attributes:
    # In general, if you are using any variable in the (rendered)
    # message, you should add it here.
    attrs = models.JSONField(encoder=DjangoJSONEncoder, default=dict)

    timestamp = models.DateTimeField(default=timezone.now)

    # References:
    # We make a polymorphic relation to stateful objects.
    ref_type = models.ForeignKey(contenttype_models.ContentType,
                                 on_delete=models.CASCADE)
    ref_id = models.PositiveIntegerField()
    ref = contenttype_fields.GenericForeignKey('ref_type', 'ref_id')

    class Meta:
        ordering = ["id"]


class StatefulMixin(models.Model):
    """
    Add state fields to a model and add reference to status
    messages.
    All stateful objects have a list of events documenting
    the state changes.
    """
    state = enumfields.EnumField(
        State,
        default=State.REQUESTED,
        max_length=255)

    status = contenttype_fields.GenericRelation(
        StatusMessage,
        content_type_field="ref_type",
        object_id_field="ref_id")

    events = contenttype_fields.GenericRelation(
        Event,
        content_type_field="ref_type",
        object_id_field="ref_id")

    class Meta:
        abstract = True


class Task(StatefulMixin, models.Model):
    """
    A task is a schedulable operation performed on
    a referenced object.

    A task holds an operation and a payload with additional
    data to successfully perform it.

    Tasks are stateful objects aswell.

    A task can be scheduled in a time window.
    """
    operation = models.CharField(max_length=255)
    payload = models.JSONField(encoder=DjangoJSONEncoder,
                               default=_initial_payload)

    created_at = models.DateTimeField(default=timezone.now)

    not_before = models.DateTimeField(default=timezone.now)
    not_after = models.DateTimeField(null=True, blank=True)

    # References:
    #
    # We make a polymorphic relation to the object we
    # are creating tasks for.
    ref_type = models.ForeignKey(contenttype_models.ContentType,
                                 on_delete=models.CASCADE)
    ref_id = models.PositiveIntegerField()
    ref = contenttype_fields.GenericForeignKey('ref_type', 'ref_id')

    class Meta:
        ordering = ["id"]
