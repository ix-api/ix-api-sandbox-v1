
"""
Events Services
"""

from jea.eventmachine.models import (
    Event,
)

def get_events(customer=None, after=0, limit=1000):
    """
    Get all events after a given serial scoped to the current
    managing customer.

    Returns a queryset with events or none.

    :param customer: The entry point customer
    :param after: The initial serial
    """
    if not customer:
        return None

    events = Event.objects.filter(customer=customer.pk,
                                  serial__gt=after)

    return events[:limit]

