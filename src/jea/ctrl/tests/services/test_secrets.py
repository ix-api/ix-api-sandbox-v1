
"""
Reset Service Tests
"""
import pytest

from jea.ctrl.services import secrets as secrets_svc


def test_generate_token():
    """Generate reset token"""
    token = secrets_svc.generate_token()
    assert token


def test_validate_token():
    """Validate a generated secret"""
    token = secrets_svc.generate_token()
    assert secrets_svc.validate(token)

    with pytest.raises(secrets_svc.InvalidTokenException):
        token += "f000"
        secrets_svc.validate(token)


