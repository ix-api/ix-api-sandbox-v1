
import os
import sys
import subprocess

from django.core.management import BaseCommand

class Command(BaseCommand):
    """Run PyTest"""
    script_path = os.path.dirname(os.path.abspath(__file__))
    project_root = os.path.abspath(
        os.path.join(script_path, "..", "..", ".."))

    cmd = ["pytest"] + sys.argv[2:]
    result = subprocess.run(
        cmd,
        cwd=project_root,
        env=os.environ,
        universal_newlines=True)

    sys.exit(result.returncode)
