
"""
Celery wrapper when using manage.py as an universal
entry point to the application.

WARNING: This is not a production ready way of
         deploying celery and should only be used
         in development.
"""

import os
import sys
import subprocess

from django.utils import autoreload
from django.core.management import BaseCommand


def restart_worker():
    script_path = os.path.dirname(os.path.abspath(__file__))
    project_root = os.path.abspath(
        os.path.join(script_path, "..", "..", "..", ".."))

    # Killall celery worker
    subprocess.run(["pkill", "-f", "celery"])

    # Start celery
    cmd = ["celery"] + sys.argv[2:] + [
        "-A", "backend",
        "worker"
    ]
    result = subprocess.run(
        cmd,
        cwd=project_root,
        env=os.environ,
        universal_newlines=True)

    sys.exit(result.returncode)


class Command(BaseCommand):
    """Run celery"""
    print("Starting background worker with autoreloader")
    autoreload.run_with_reloader(restart_worker)

