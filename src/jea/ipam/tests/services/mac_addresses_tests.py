
"""
Test mac addresses service
"""

import pytest
from model_bakery import baker
from django.utils import timezone
from django.core.exceptions import ValidationError

from jea.exceptions import ResourceAccessDenied
from jea.ipam.services import mac_addresses as mac_addresses_svc
from jea.ipam.models import MacAddress


@pytest.mark.django_db
def test_list_mac_addresses__base_queryset():
    """
    Get base queryset for customer mac addresses
    """
    c1 = baker.make("crm.Customer")
    c2 = baker.make("crm.Customer", scoping_customer=c1)

    sc1 = baker.make("access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c1,
        managing_customer=c1,
        consuming_customer=c2)
    sc2 = baker.make("access.ClosedUserGroupNetworkServiceConfig",
        scoping_customer=c1,
        managing_customer=c1,
        consuming_customer=c2)
    sc3 = baker.make("access.ClosedUserGroupNetworkServiceConfig")

    # Assign mac addresses
    mac1 = baker.make("ipam.MacAddress",
        scoping_customer=c1,
        managing_customer=c1,
        consuming_customer=c2)
    mac1.exchange_lan_network_service_configs.add(sc1)

    mac2 = baker.make("ipam.MacAddress",
        scoping_customer=c1,
        managing_customer=c1,
        consuming_customer=c2)
    mac2.closed_user_group_network_service_configs.add(sc2)
    mac3 = baker.make("ipam.MacAddress",
        scoping_customer=c2,
        managing_customer=c1,
        consuming_customer=c1)
    mac3.closed_user_group_network_service_configs.add(sc3)

    # Get customer mac addresses
    mac_addresses = mac_addresses_svc.get_mac_addresses(
        scoping_customer=c1)

    assert mac1 in mac_addresses
    assert mac2 in mac_addresses
    assert not mac3 in mac_addresses


@pytest.mark.django_db
def test_get_mac_address():
    """Get a single mac address"""
    m1 = baker.make("crm.Customer")
    c1 = baker.make("crm.Customer", scoping_customer=m1)
    c2 = baker.make("crm.Customer", scoping_customer=m1)

    sc1 = baker.make("access.ExchangeLanNetworkServiceConfig",
        scoping_customer=m1,
        managing_customer=c1,
        consuming_customer=c1)
    sc2 = baker.make("access.ClosedUserGroupNetworkServiceConfig",
        scoping_customer=m1,
        managing_customer=c2,
        consuming_customer=c2)

    # Assign mac addresses
    mac1 = baker.make("ipam.MacAddress",
        scoping_customer=m1,
        managing_customer=c1,
        consuming_customer=c1)
    mac2 = baker.make("ipam.MacAddress",
        scoping_customer=m1,
        managing_customer=c2,
        consuming_customer=c2)

    # Query mac addresses
    assert mac_addresses_svc.get_mac_address(
        scoping_customer=m1,
        mac_address=mac1) == mac1

    assert mac_addresses_svc.get_mac_address(
        scoping_customer=m1,
        mac_address=mac1.pk) == mac1

    with pytest.raises(MacAddress.DoesNotExist):
        mac_addresses_svc.get_mac_address(
            scoping_customer=c2,
            mac_address=mac1.pk)

    with pytest.raises(ResourceAccessDenied):
        mac_addresses_svc.get_mac_address(
            scoping_customer=c2,
            mac_address=mac1)


@pytest.mark.django_db
def test_create_mac_address():
    """Test creating a mac address"""
    c = baker.make("crm.Customer")
    mac = mac_addresses_svc.create_mac_address(
        mac_address_input={
            "address": "11:22:33:44:55:66",
            "valid_not_before": timezone.now(),
            "valid_not_after": None,
            "external_ref": "f00",
            "managing_customer": c.pk,
            "consuming_customer": c.pk,
        },
        scoping_customer=c)

    assert mac
    assert mac.pk


@pytest.mark.django_db
def test_assign_mac_address__exchange_lan_network_service():
    """Assign a mac address to a service (config)"""
    c = baker.make("crm.Customer")
    sc1 = baker.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=c, managing_customer=c, consuming_customer=c)
    mac = baker.make(
        "ipam.MacAddress",
        scoping_customer=sc1.scoping_customer,
        managing_customer=sc1.scoping_customer,
        consuming_customer=sc1.scoping_customer)

    mac_address = mac_addresses_svc.assign_mac_address(
        mac_address_input={
            "mac_address": mac.pk,
            "network_service_config": sc1,
        },
        scoping_customer=sc1.scoping_customer)

    assert sc1 in mac_address.exchange_lan_network_service_configs.all()


@pytest.mark.django_db
def test_remove_mac_address():
    """Remove a mac address"""
    s = baker.make("access.ExchangeLanNetworkServiceConfig")
    mac = baker.make("ipam.MacAddress",
        exchange_lan_network_service_configs=[s])

    with pytest.raises(ValidationError):
        mac_addresses_svc.remove_mac_address(
            mac_address=mac)

    s.mac_addresses.clear()
    mac = mac_addresses_svc.remove_mac_address(
        mac_address=mac)

    assert not mac.pk
