
"""
IPAM events
"""

from jea.eventmachine.models import Event


IP_ADDRESS_ALLOCATED = "@ipam/ip_address_allocated"
IP_ADDRESS_RELEASED = "@ipam/ip_address_released"

MAC_ADDRESS_CREATED = "@ipam/mac_address_created"
MAC_ADDRESS_ASSIGNED = "@ipam/mac_address_assigned"
MAC_ADDRESS_REMOVED = "@ipam/mac_address_removed"


def ip_address_allocated(ip_address):
    """Ip address was allocated"""
    return Event(
        type=IP_ADDRESS_ALLOCATED,
        payload={
            "ip_address_id": ip_address.pk,
            "managing_customer": ip_address.managing_customer.pk,
            "consuming_customer": ip_address.consuming_customer.pk,
        },
        ref=ip_address,
        customer=ip_address.scoping_customer)


def ip_address_released(ip_address):
    return Event(
        type=IP_ADDRESS_RELEASED,
        payload={
            "ip_address_id": ip_address.pk,
            "managing_customer": ip_address.managing_customer.pk,
            "consuming_customer": ip_address.consuming_customer.pk,
        },
        customer=ip_address.scoping_customer)


def mac_address_assigned(mac_address):
    return Event(
        type=MAC_ADDRESS_ASSIGNED,
        payload={
            "mac_address_id": mac_address.pk,
            "managing_customer": mac_address.managing_customer.pk,
            "consuming_customer": mac_address.consuming_customer.pk,
        },
        ref=mac_address,
        customer=mac_address.scoping_customer)


def mac_address_removed(mac_address):
    return Event(
        type=MAC_ADDRESS_REMOVED,
        payload={
            "mac_address_id": mac_address.pk,
            "managing_customer": mac_address.managing_customer.pk,
            "consuming_customer": mac_address.consuming_customer.pk,
        },
        customer=mac_address.scoping_customer)


def mac_address_created(mac_address):
    return Event(
        type=MAC_ADDRESS_CREATED,
        payload={
            "mac_address_id": mac_address.pk,
            "managing_customer": mac_address.managing_customer.pk,
            "consuming_customer": mac_address.consuming_customer.pk,
        },
        ref=mac_address,
        customer=mac_address.scoping_customer)

