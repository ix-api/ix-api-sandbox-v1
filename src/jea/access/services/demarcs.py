
"""
Demarcs Service
---------------

Handles allocation and releasing of (port) demarcation points.
"""

import re 
from typing import Optional, Iterable

from rest_framework.exceptions import APIException
from django.db import transaction
from django.utils import text as text_utils

from jea.exceptions import ResourceAccessDenied, ValidationError
from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.catalog.services import pops as pops_svc
from jea.crm.services import (
    contacts as contacts_svc,
    customers as customers_svc,
)
from jea.catalog.models import (
    PointOfPresence,
)
from jea.access.filters import (
    DemarcationPointFilter,
)
from jea.access.models import (
    MediaType,
    DemarcationPoint
)
from jea.access.services import connections as connections_svc
from jea.crm.models import (
    Customer,
)
from jea.access.events import (
    demarc_updated,
    demarc_allocated,
    demarc_requested,
    demarc_released,
    demarc_state_changed,
)
from jea.access.exceptions import (
    DemarcationPointUnavailable,
    DemarcationPointInUse,
    DemarcationPointNotReady,
    DemarcationPointNotConnected,
)


RE_MATCH_SPEED = re.compile(r"(\d+)(\w)?BASE")


class UnknownMediaTypeError(ValueError):
    pass


def max_speed_for_media_type(media_type: str) -> int:
    """
    Get max speed from media type

    :raises UnknownMediaTypeError: when media type could not be parsed
    """
    match = RE_MATCH_SPEED.match(media_type.upper())
    if not match:
        raise UnknownMediaTypeError

    matched_speed, prefix = match.groups()
    try:
        speed = int(matched_speed)
    except:
        raise UnknownMediaTypeError

    multipliers = {
        None: 1,
        "G": 1000,
        "T": 1000000,
    }

    try:
        return speed * multipliers[prefix]
    except:
        raise UnknownMediaTypeError


def make_demarc_name(customer=None) -> str:
    """Make a demarc name"""
    # When a customer is present, limit to customer owned demarcs and
    # include customer in the naming schema: demarc-customer-name-32
    if customer:
        counter = customer.consumed_demarcationpoints.count() + 1
        tag = text_utils.slugify(customer.name)[:42]
        return f"demarc-{tag}-{counter}"

    # Fallback
    counter = Connection.objects.count() + 1
    return f"demarc-{counter}"


#
# Service Implementation
#

@active.command
def allocate_demarcation_point(
        dispatch,
        scoping_customer=None,
        demarcation_point_input=None,
    ) -> DemarcationPoint:
    """
    Allocate a port demarcation for a given customer
    at a demarcation point.

    :scoping_customer: The managing customer
    :param demarcation_point: The referenced demarcation point.
    :param demarcation_point_input: User input like name and speed

    :returns: A new port demarcation point
    """
    if not demarcation_point_input:
        raise ValidationError("Missing demarc_input")

    # Setup related dependencies
    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=demarcation_point_input.get("consuming_customer"))
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=demarcation_point_input.get("managing_customer"))
    if not consuming_customer:
        raise ValidationError("Owning customer missing")
    if not managing_customer:
        raise ValidationError("Billing customer missing")

    pop = demarcation_point_input["point_of_presence"]
    connection = demarcation_point_input.get("connection")

    # Determine port speed if only the type is given
    demarc_type = demarcation_point_input["media_type"].upper()
    demarc_max_speed = max_speed_for_media_type(demarc_type)
    demarc_name = make_demarc_name(customer=consuming_customer)

    # Check if the requested port demarc is in priciple available
    # at the demarcation point
    count = pops_svc.get_media_type_availability(
        point_of_presence=pop,
        media_type=demarc_type,
        presence="physical")
    if count < 1:
        raise DemarcationPointUnavailable(
            ("There is no free slot available at the given pop."),
            field="pop")


    # Check contacts: We allow both contacts from the
    # billing and the owning customer. The required type
    # is implemention an billing.
    contacts = [
        contacts_svc.get_contact(
            scoping_customer=scoping_customer,
            contact=c)
        for c in demarcation_point_input["contacts"]]

    # We require at least a billing and an implementation contact
    contacts_svc.assert_presence_of_contact_types(
        required_contact_types=[
            "implementation", "billing",
        ],
        contacts=contacts,
        scoping_customer=scoping_customer,
    )

    # Okay - Looking good. Let's create a new port demarc!
    with transaction.atomic():
        demarc = DemarcationPoint(
            state=State.REQUESTED,
            name=demarc_name,
            media_type=demarc_type,
            speed=demarc_max_speed,
            scoping_customer=scoping_customer,
            managing_customer=managing_customer,
            consuming_customer=consuming_customer,
            purchase_order=demarcation_point_input.get("purchase_order", ""),
            external_ref=demarcation_point_input.get("external_ref"),
            contract_ref=demarcation_point_input.get("contract_ref"),
            point_of_presence=pop)
        demarc.save()

        # Assign contacts
        demarc.contacts.set(contacts)

        # Dispatch create event and refresh in case of state changes
        dispatch(demarc_requested(demarc)) # My man.
        demarc.refresh_from_db()

        # Connect demarc if a connection is present
        if connection:
            connections_svc.connect_demarcation_point(
                connection=connection,
                demarcation_point=demarc,
                scoping_customer=scoping_customer)


    return demarc


@active.command
def release_demarcation_point(
        dispatch,
        demarcation_point=None,
        scoping_customer=None,
    ):
    """
    Deallocate a port demarc. This will fail if the port demarc
    is still used in a connection.

    :param demarcation_point: The port demarcation point to release
    """
    demarcation_point = get_demarcation_point(
        scoping_customer=scoping_customer,
        demarcation_point=demarcation_point)

    if demarcation_point.connection:
        raise DemarcationPointInUse

    # Remove port demarc
    prev_state = demarcation_point.state
    demarcation_point.state = State.DECOMMISSIONED
    demarcation_point.save()

    # Inform subscribers
    dispatch(demarc_state_changed(
        demarcation_point,
        prev_state=prev_state,
        next_state=State.DECOMMISSIONED))

    dispatch(demarc_released(demarcation_point))
    demarcation_point.refresh_from_db()

    return demarcation_point


@active.command
def archive_demarcation_point(
        dispatch,
        scoping_customer=None,
        demarcation_point=None,
    ):
    """
    Archive port demarc. This is only okay if the
    state is decommissioned.
    Archived port demarcs are not included in the default set.

    :param demarcation_point: The port demarcation point to archive.
    """
    demarcation_point = get_demarcation_point(
        scoping_customer=scoping_customer,
        demarcation_point=demarcation_point)

    if not demarcation_point.state == State.DECOMMISSIONED:
        raise DemarcationPointInUse

    demarcation_point.state = State.ARCHIVED
    demarcation_point.save()

    # Inform subscribers
    dispatch(demarc_state_changed(
        demarcation_point,
        prev_state=State.DECOMMISSIONED,
        next_state=State.ARCHIVED))

    return demarcation_point


def get_demarcation_points(
        scoping_customer=None,
        filters=None,
    ) -> Iterable[DemarcationPoint]:
    """
    Get all port demarcation points for a given managing customer.

    :param scoping_customer: A customer managing demarcs
    :param filters: A dict with filters from query
    """
    # Get filtered demarcs query set
    if filters:
        demarcs = DemarcationPointFilter(filters).qs
    else:
        demarcs = DemarcationPoint.objects.all()

    # Apply additional customer filtering
    if scoping_customer:
        demarcs = demarcs.filter(scoping_customer=scoping_customer)

    return demarcs


def get_demarcation_point(
        scoping_customer=None,
        demarcation_point=None,
    ) -> Optional[DemarcationPoint]:
    """
    Get a single port demarc. Limit set of valid demarcs to
    managing customer's scope.

    :raises: ResourceAccessDenied

    :param scoping_customer: The managing customer of the port demarc
    :param demarcation_point: The port demarc identifier.
    """
    if not demarcation_point:
        raise ValidationError(
            "A demarcation point is required")

    # We have a given demarc, so no lookup is required
    # however, we perform a check on the scoping:
    if not isinstance(demarcation_point, DemarcationPoint):
        # Resolve demarc by id
        demarcation_point = DemarcationPoint.objects.get(pk=demarcation_point)

    # Check ownership
    if scoping_customer and not \
        demarcation_point.scoping_customer == scoping_customer:
        raise ResourceAccessDenied(demarcation_point)

    return demarcation_point # We are done here.


def demarcation_point_can_join_connection(
        scoping_customer=None,
        demarcation_point=None,
        connection=None
    ):
    """
    Check if a demarcation point can join a connection.
      - A demarc needs to be at the same pop as the others.
      - The state should be just allocated or requested.
        Not in production or decomissioned.

    This will raise a validation error.

    :raises DemarcationPointInUse: In case the demarc is already
        in a connection.
    :raises DemarcationPointUnavailable: In case the demarc can not
        join the connection.
    """
    demarc = get_demarcation_point(
        demarcation_point=demarcation_point,
        scoping_customer=scoping_customer)
    connection = connections_svc.get_connection(
        connection=connection,
        scoping_customer=scoping_customer)

    # Check if we are in use
    if demarc.connection and demarc.connection != connection:
        raise DemarcationPointInUse(field="connection")

    # Check State
    if demarc.state not in [State.ALLOCATED, State.REQUESTED]:
        raise DemarcationPointNotReady(field="connection")

    # Check related demarcs
    connected_demarc = connection.demarcation_points.first()
    if connected_demarc:
        # Check if we are at the same pop
        if demarcation_point.point_of_presence_id != \
            connected_demarc.point_of_presence_id:
            raise DemarcationPointUnavailable(
                ("The demarc is located a different pop and can not "
                 "join the connection."),
                field="connection")


@transaction.atomic
@active.command
def update_demarcation_point(
        dispatch,
        scoping_customer=None,
        demarcation_point=None,
        demarcation_point_update=None,
    ):
    """
    Update a demarcation point.

    This includes assigning a demarc to a connection, or
    disconnecting a demarc from a connection by setting it
    to None.
    """
    demarcation_point = get_demarcation_point(
        demarcation_point=demarcation_point,
        scoping_customer=scoping_customer)

    # Speed (for ratelimmiting):
    if "speed" in demarcation_point_update.keys():
        demarcation_point.speed = demarcation_point_update["speed"]

    # Implementation contacts:
    if "contacts" in demarcation_point_update.keys():
        contacts = [
            contacts_svc.get_contact(
                scoping_customer=scoping_customer,
                contact=c)
            for c in demarcation_point_update["contacts"]]
        demarcation_point.contacts.set(contacts)

    # Ownership
    managing_customer = demarcation_point_update.get("managing_customer")
    if managing_customer:
        managing_customer = customers_svc.get_customer(
            customer=managing_customer,
            scoping_customer=scoping_customer)
        demarcation_point.managing_customer = managing_customer

    consuming_customer = demarcation_point_update.get("consuming_customer")
    if consuming_customer:
        consuming_customer = customers_svc.get_customer(
            customer=consuming_customer,
            scoping_customer=scoping_customer)
        demarcation_point.consuming_customer = consuming_customer

    # Purchase Order / External Ref / Contract Ref
    # TODO: this should be refactored and generalized.
    if "purchase_order" in demarcation_point_update.keys():
        demarcation_point.purchase_order = \
            demarcation_point_update["purchase_order"]
    if "external_ref" in demarcation_point_update.keys():
        demarcation_point.external_ref = \
            demarcation_point_update["external_ref"]
    if "contract_ref" in demarcation_point_update.keys():
        demarcation_point.contract_ref = \
            demarcation_point_update["contract_ref"]

    # Persist changes and trigger statemachine
    demarcation_point.save()
    dispatch(demarc_updated(demarcation_point))
    demarcation_point.refresh_from_db()

    # Connect or disconnect the demarc
    if "connection" in demarcation_point_update.keys():
        connection = demarcation_point_update.get("connection")
        if connection:
            connections_svc.connect_demarcation_point(
                scoping_customer=scoping_customer,
                connection=connection,
                demarcation_point=demarcation_point)
        else:
            connections_svc.disconnect_demarcation_point(
                scoping_customer=scoping_customer,
                demarcation_point=demarcation_point)

        demarcation_point.refresh_from_db()

    return demarcation_point

