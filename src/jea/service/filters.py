
"""
Service Filters
"""

import django_filters
from django.db import models

from utils import filters
from jea.crm import filters as crm_filters
from jea.service import models as service_models
from jea.service.models import (
   NetworkService,
   ExchangeLanNetworkService,
   ClosedUserGroupNetworkService,
   ELineNetworkService,
   CloudNetworkService,
   NetworkFeature,

   INV_SERVICE_TYPES,
   INV_FEATURE_TYPES,
)


class NetworkServiceFilter(
        crm_filters.OwnableFilterMixin,
        django_filters.FilterSet,
    ):
    """Filter network services"""
    id = filters.BulkIdFilter()

    pop = django_filters.CharFilter(method="filter_pop")
    def filter_pop(self, queryset, _name, value):
        """Filter by pop"""
        # TODO: Implement something meaningfull in here.
        #       What that is is still to be discussed
        return queryset


    TYPE_CHOICES = (
        (service_models.SERVICE_TYPE_EXCHANGE_LAN,
         service_models.SERVICE_TYPE_EXCHANGE_LAN),
        (service_models.SERVICE_TYPE_CLOSED_USER_GROUP,
         service_models.SERVICE_TYPE_CLOSED_USER_GROUP),
        (service_models.SERVICE_TYPE_ELINE,
         service_models.SERVICE_TYPE_ELINE),
        (service_models.SERVICE_TYPE_CLOUD,
         service_models.SERVICE_TYPE_CLOUD),
    )

    #
    # Polymorphic Type Filter
    #
    type = django_filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)

    def filter_type(self, queryset, _name, value):
        """Filter polymorphic by type"""
        service_class = INV_SERVICE_TYPES.get(value)
        if not service_class:
            return queryset.none()

        # Apply filter
        return queryset.instance_of(service_class)

    #
    # Fields
    #

    class Meta:
        model = NetworkService
        fields = [
            "product",
        ]



class NetworkFeatureFilter(django_filters.FilterSet):
    id = filters.BulkIdFilter()

    # TODO: Generalize this fix.
    required = django_filters.CharFilter(method="filter_required")
    def filter_required(self, queryset, _name, value):
        """Filter required network features"""
        value = value.lower()
        bool_value = False
        if value == "true" or value=="1":
            bool_value = True

        return queryset.filter(required=bool_value)


    network_service = django_filters.CharFilter()

    """
    TYPE_CHOICES = (
        (service_models.FEATURE_TYPE_BLACKHOLING,
         service_models.FEATURE_TYPE_BLACKHOLING),
        (service_models.FEATURE_TYPE_ROUTESERVER,
         service_models.FEATURE_TYPE_ROUTESERVER),
        (service_models.FEATURE_TYPE_IXPROUTER,
         service_models.FEATURE_TYPE_IXPROUTER),
    )
    """
    TYPE_CHOICES = (
        (service_models.FEATURE_TYPE_ROUTESERVER,
         service_models.FEATURE_TYPE_ROUTESERVER),
    )

    #
    # Polymorphic Type Filter
    #
    type = django_filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)

    def filter_type(self, queryset, _name, value):
        """Filter polymorphic by type"""
        service_class = INV_FEATURE_TYPES.get(value)
        if not service_class:
            return queryset.none()

        # Apply filter
        return queryset.instance_of(service_class)

    class Meta:
        model = NetworkFeature
        fields = [
            "name",
        ] 

