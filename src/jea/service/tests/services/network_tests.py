
"""
Test network service
"""

import pytest
from model_bakery import baker

from jea.service.services import network as network_svc
from jea.service import exceptions


@pytest.mark.django_db
def test_get_network_services():
    """Test listing network services"""
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    closed_user_group = baker.make("service.ClosedUserGroupNetworkService")
    eline = baker.make("service.ELineNetworkService")
    cloud = baker.make("service.CloudNetworkService")

    services = network_svc.get_network_services()

    assert exchange_lan in services
    assert closed_user_group in services
    assert eline in services
    assert cloud in services


@pytest.mark.django_db
def test_get_network_services_filter_type():
    """Test listing network services filtered by type"""
    exchange_lan = baker.make("service.ExchangeLanNetworkService")
    closed_user_group = baker.make("service.ClosedUserGroupNetworkService")
    eline = baker.make("service.ELineNetworkService")
    cloud = baker.make("service.CloudNetworkService")

    # Exchange Lan
    services = network_svc.get_network_services(filters={
        "type": "exchange_lan",
    })
    assert exchange_lan in services
    assert not closed_user_group in services
    assert not eline in services
    assert not cloud in services

    # Closed User Group
    services = network_svc.get_network_services(filters={
        "type": "closed_user_group",
    })
    assert not exchange_lan in services
    assert closed_user_group in services
    assert not eline in services
    assert not cloud in services

    # ELine
    services = network_svc.get_network_services(filters={
        "type": "eline",
    })
    assert not exchange_lan in services
    assert not closed_user_group in services
    assert eline in services
    assert not cloud in services

    # Cloud
    services = network_svc.get_network_services(filters={
        "type": "cloud",
    })
    assert not exchange_lan in services
    assert not closed_user_group in services
    assert not eline in services
    assert cloud in services


@pytest.mark.django_db
def test_get_network_service__by_service_id():
    """Test getting a single service by id"""
    service = baker.make("service.ExchangeLanNetworkService")
    service_ = network_svc.get_network_service(
        network_service=str(service.pk)) 
    assert service.pk == service_.pk

    with pytest.raises(Exception):
        network_svc.get_network_service()


@pytest.mark.django_db
def test_get_network_features():
    """Get all the network features"""
    feature = baker.make("service.RouteServerNetworkFeature")
    features = network_svc.get_network_features()
    assert feature in features

    # TODO: Add tests for filtering

@pytest.mark.django_db
def test_get_network_feature__cached():
    """Get a feature"""
    feature = baker.make("service.RouteServerNetworkFeature")
    result = network_svc.get_network_feature(
        network_feature=feature)

    assert result == feature


@pytest.mark.django_db
def test_get_network_feature__cached():
    """Get a feature"""
    feature = baker.make("service.RouteServerNetworkFeature")
    result = network_svc.get_network_feature(
        network_feature=feature.pk)

    assert result == feature


@pytest.mark.django_db
def test_assert_network_feature_available():
    """Test availability assertion for a given network feature"""
    s1 = baker.make("service.ExchangeLanNetworkService")
    s2 = baker.make("service.ExchangeLanNetworkService")
    f1 = baker.make("service.BlackholingNetworkFeature",
                     network_service=s1)
    f2 = baker.make("service.BlackholingNetworkFeature",
                    network_service=s2)

    # Network feature is available:
    network_svc.assert_network_feature_available(f1, s1)

    # Feature is not available
    with pytest.raises(exceptions.NetworkFeatureNotAvailable):
        network_svc.assert_network_feature_available(f2, s1)

