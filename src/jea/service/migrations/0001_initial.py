# Generated by Django 4.0.3 on 2022-03-09 09:18

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion
import enumfields.fields
import ixapi_schema.openapi.components
import ixapi_schema.v1.constants.access
import jea.service.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('crm', '0001_initial'),
        ('catalog', '0001_initial'),
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='NetworkFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=80)),
                ('required', models.BooleanField()),
                ('required_contact_types', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=30), blank=True, default=jea.service.models.NetworkFeature._default_contact_types, size=None)),
            ],
            options={
                'abstract': False,
                'base_manager_name': 'objects',
            },
        ),
        migrations.CreateModel(
            name='NetworkService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('external_ref', models.CharField(max_length=128, null=True)),
                ('purchase_order', models.CharField(max_length=80)),
                ('contract_ref', models.CharField(max_length=128, null=True)),
                ('required_contact_types', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=30), blank=True, default=jea.service.models.NetworkService._default_contact_types, size=None)),
                ('consuming_customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='consumed_%(class)ss', to='crm.customer')),
                ('contacts', models.ManyToManyField(related_name='%(class)ss', to='crm.contact')),
                ('managing_customer', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='managed_%(class)ss', to='crm.customer')),
                ('polymorphic_ctype', models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_%(app_label)s.%(class)s_set+', to='contenttypes.contenttype')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='network_services', to='catalog.product')),
                ('scoping_customer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_%(class)ss', to='crm.customer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BlackholingNetworkFeature',
            fields=[
                ('networkfeature_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='service.networkfeature')),
            ],
            options={
                'verbose_name': 'Blackholing Feature',
            },
            bases=('service.networkfeature',),
        ),
        migrations.CreateModel(
            name='ClosedUserGroupNetworkService',
            fields=[
                ('networkservice_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='service.networkservice')),
            ],
            options={
                'verbose_name': 'Closed UserGroup Network Service',
            },
            bases=('service.networkservice',),
        ),
        migrations.CreateModel(
            name='ELineNetworkService',
            fields=[
                ('networkservice_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='service.networkservice')),
            ],
            options={
                'verbose_name': 'E-Line Network Service',
            },
            bases=('service.networkservice',),
        ),
        migrations.CreateModel(
            name='ExchangeLanNetworkService',
            fields=[
                ('networkservice_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='service.networkservice')),
                ('name', models.CharField(max_length=40)),
                ('metro_area', models.CharField(max_length=3)),
                ('peeringdb_ixid', models.PositiveIntegerField(blank=True, null=True)),
                ('ixfdb_ixid', models.PositiveIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'ExchangeLAN Network Service',
            },
            bases=('service.networkservice',),
        ),
        migrations.CreateModel(
            name='IXPRouterNetworkFeature',
            fields=[
                ('networkfeature_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='service.networkfeature')),
                ('asn', models.PositiveIntegerField()),
                ('fqdn', models.CharField(max_length=80)),
            ],
            options={
                'verbose_name': 'IXPRouter Feature',
            },
            bases=('service.networkfeature',),
        ),
        migrations.CreateModel(
            name='RouteServerNetworkFeature',
            fields=[
                ('networkfeature_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='service.networkfeature')),
                ('asn', models.PositiveIntegerField(null=True)),
                ('fqdn', models.CharField(max_length=80)),
                ('looking_glass_url', models.URLField(blank=True, null=True)),
                ('address_families', django.contrib.postgres.fields.ArrayField(base_field=enumfields.fields.EnumField(enum=ixapi_schema.openapi.components.AddressFamilies, max_length=10), size=None)),
                ('session_mode', enumfields.fields.EnumField(enum=ixapi_schema.v1.constants.access.RouteServerSessionMode, max_length=20)),
                ('available_bgp_session_types', django.contrib.postgres.fields.ArrayField(base_field=enumfields.fields.EnumField(enum=ixapi_schema.v1.constants.access.BGPSessionType, max_length=20), size=None)),
            ],
            options={
                'verbose_name': 'RouteServer Feature',
            },
            bases=('service.networkfeature',),
        ),
        migrations.AddField(
            model_name='networkfeature',
            name='network_service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='network_features', to='service.networkservice'),
        ),
        migrations.AddField(
            model_name='networkfeature',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_%(app_label)s.%(class)s_set+', to='contenttypes.contenttype'),
        ),
        migrations.CreateModel(
            name='IXPSpecificFeatureFlag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('description', models.CharField(max_length=80)),
                ('network_feature', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ixp_specific_flags', to='service.networkfeature')),
            ],
            options={
                'verbose_name': 'IXP-specific feature flag',
            },
        ),
        migrations.CreateModel(
            name='CloudNetworkService',
            fields=[
                ('networkservice_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='service.networkservice')),
                ('cloud_provider', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='cloud_network_services', to='catalog.cloudprovider')),
            ],
            options={
                'verbose_name': 'Cloud Network Service',
            },
            bases=('service.networkservice',),
        ),
    ]
