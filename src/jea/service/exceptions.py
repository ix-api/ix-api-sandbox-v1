
"""
Service Exceptions
"""

from django.core.exceptions import ValidationError


class NetworkServiceNotAvailable(ValidationError):
    """A network service is not available"""

    def __init__(self, service):
        msg = "Network service '{}' is not available.".format(
            service.__class__.__name__)

        super(NetworkServiceNotAvailable, self).__init__(msg)



class NetworkFeatureNotAvailable(ValidationError):
    """A network feature is not available"""
    def __init__(self, network_feature, network_service=None):
        """Initialize exception"""
    
        if network_service:
            msg = "Network service '{}' is not available for '{}'.".format(
                network_feature.__class__.__name__,
                network_service.__class__.__name__)
        else:
            msg = "Network feature '{}' is not available.".format(
                network_feature.__class__.__name__)

        super(NetworkFeatureNotAvailable, self).__init__(msg)


