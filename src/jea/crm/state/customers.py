
"""
Customer State Event Handler

Reacts to events like customer created and
contact created.
"""

from django.core.exceptions import ValidationError

from jea.eventmachine import active
from jea.eventmachine.models import (
    State,
)
from jea.crm.status import (
    customer_contact_missing,
)
from jea.crm.events import (
    CUSTOMER_CREATED,
    CONTACT_CREATED,
    CONTACT_DELETED,

    customer_state_changed,
)
from jea.crm.models import (
    Customer,
    Contact,
    LegalContact,
    NocContact,
    ImplementationContact,
)


@active.handler(
    CUSTOMER_CREATED,
    CONTACT_CREATED,
    CONTACT_DELETED,
)
def update_customer_state(dispatch, event):
    """
    Update customer state when the customer is created
    and contacts are added
    """
    if isinstance(event.ref, Contact):
        customer = event.ref.consuming_customer
    elif isinstance(event.ref, Customer):
        customer = event.ref
    else:
        raise ValidationError("Unsupport customer reference")

    # Check if customer has all required contacts
    legal_contacts = customer.consumed_contacts.instance_of(LegalContact)

    # Reset status
    customer.status.clear()
    next_state = State.PRODUCTION # optimist.

    if legal_contacts.count() == 0:
        next_state = State.ERROR
        status_message = customer_contact_missing(customer, "legal")
        status_message.save()

    # Check state change
    if customer.state != next_state:
        prev_state = customer.state
        customer.state = next_state
        customer.save()
        dispatch(customer_state_changed(customer, prev_state, next_state))
