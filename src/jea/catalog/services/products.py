
"""
Products Service
"""

from typing import Optional

from django.db.models.query import QuerySet

from jea.eventmachine import active
from jea.catalog.models import (
    Product,
)
from jea.catalog.filters import (
    ProductFilter,
)


def get_products(scoping_customer=None, filters=None) -> QuerySet:
    """
    List and filter products.
    This returns a polymorphic query set including:
     - ExchangeLanProducts,
     - ClosedUserGroupProducts,
     - ELineProducts,
     - CloudProducts
    """
    filtered = ProductFilter(filters)
    return filtered.qs


def get_product(scoping_customer=None, product=None) -> Product:
    """
    Retrieve a single product identified by a unique
    property. In this case: The primary key / id.

    :param product: The id of the product to fetch.
    :raises Product.DoesNotExist: When the product is not found
    """
    if isinstance(product, Product):
        # Maybe trigger a refresh here?
        return product

    return Product.objects.get(pk=product)

