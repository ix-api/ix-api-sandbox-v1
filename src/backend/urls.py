"""
JEA URL Configuration
"""
from django.urls import include, path

from jea import admin
from jea.public import urls as public_urls
from jea.api.v1 import urls as api_v1_urls
from jea.api.ctrl import urls as api_ctrl_urls

urlpatterns = [
    path(r"", include(public_urls)),
    path(r"admin/", admin.site.urls),
    path(r"api/v1/", include(api_v1_urls)),
    path(r"api/ctrl/", include(api_ctrl_urls)),
]

