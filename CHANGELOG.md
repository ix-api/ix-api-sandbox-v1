# 1.3.4 (2022-03-09)

Upgraded all dependencies. Please note, that it is required
to recreate the database and apply migrations and bootstrapping again.

# 0.0.1 - Initial Commit

