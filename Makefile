
#
# JEA :: SANDBOX
# --------------
#
# Makefile for project tasks
#

VERSION ?= dev

volumes:
	docker volume create --name=ixapi-sandbox-v1_postgres_dev

#
# Production / Deployment
#

clean:
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

up: volumes
	docker-compose  up

migrate:
	./bin/sandbox migrate

superuser:
	./bin/sandbox createsuperuser

bootstrap:
	./bin/sandbox bootstrap
