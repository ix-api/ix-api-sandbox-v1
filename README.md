# Joint External API - Sandbox - V1

This is a reference implementation of the `v1` version of the ix-api.
This sandbox implements the `v1` the ix-api schema: https://gitlab.com/ix-api/ix-api-schema

## Running the Sandbox 

The easiest way to run the sandbox is to clone the
repository and use docker-compose.

    git clone https://gitlab.com/ix-api/ix-api-sandbox-v1.git

    make up

Ensure the Sandbox database is up-to-date

    make migrate

Create a superuser account for the admin interface. (An email is not required and can just be skipped.)

    make superuser

Populate the Sandbox with mock data

    make bootstrap

The Sandbox is now running at [http://localhost:8000](http://localhost:8000)
    

The `bin/sandbox` wrapper around docker-compose can be used to
easily run commands in a sandbox container.



### Testing

Run all tests in the docker container using pytest

    ./bin/sandbox pytest

Add flags for more verbose output

    ./bin/sandbox pytest -s -v


## Project Structure

All sanbox related apps are located within the `src/jea` module.
The sandbox is split into the following apps:

 - `jea.public` A minimal landing page
 - `jea.auth` The authentication backend application providing authentication services
 - `jea.catalog` The service catalog
 - `jea.crm` All crm related services
 - `jea.service` Package for network services and features
 - `jea.api.v1` The api interface implementation of the IX-API

### Tests

Tests are grouped by app and a `tests/` directory is located in the
first level of the application's root (e.g. `src/jea/api/v1/tests/`).

The test folder structure should reflect the structure of the app.

### The `jea.api.v1` application

The API application reflects to some degree the overall project structure.

All resources are grouped in their specific module: A resource usually
consists of a `views.py` and a `serializers.py`. Additional resource
related modules should be placed within the directory of the resource.

Example:
    
    src/jea/api/v1/crm/serializers.py
    src/jea/api/v1/crm/views.py


## Bootstrapping

The JEA sandbox comes with management command to setup a
demonstration / testing environment with a populated catalog
and test customers.

This is done by running the `jea.ctrl.managment` command: `bootstrap`.

### Commandline Arguments

You can provide various settings directly on invocation.
If some information is missing, bootstrap will ask you for it
or will generate some made up values.

    --yes
        Assume yes when asked, e.g. when clearing the database

    --api-key
        Provide a fixed api key

    --api-secret
        Provide a fixed api secret for the reseller customer

    --exchange-name
        The name of your exchange.

        Bootstrap will prompt for input when in interactive mode, 
        or will suggest some random name.

    --exchange-asn
        The IXPs ASN.

        Bootstrap will prompt for input or will make
        some ASN up from the private AS range.

    --api-customer
        The name of the reseller / API customer

    --defaults
        Don't prompt for any input, just make something up in case
        it's not provided by the above flags.
