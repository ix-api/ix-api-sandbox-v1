
#
# Dockerfile for IX-API Sandbox v1
#

FROM python:3.10
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
  apt-get install --no-install-recommends -y \
  inetutils-ping \
  netcat \
  postgresql-client && \
  rm -rf /var/lib/apt/lists/*

# Setup project
RUN mkdir -p /code
COPY . /code

WORKDIR /code
RUN pip3 install -r requirements/requirements.txt

ENTRYPOINT ["python3", "src/manage.py"]

